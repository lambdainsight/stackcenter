function manageCookie(){
	var checkCookie = getCookie("li_cookie_consent");
	var boxTarget = document.getElementById("cookieBox");
	if(checkCookie != "true"){
		boxTarget.style.display = 'flex';
		boxTarget.addEventListener("click", closeCookieWindow);
	}
	setTimeout(
		function() {
			fadeOutWindow();
	}, 10000);
}
function closeCookieWindow(){
	var checkCookie = getCookie("li_cookie_consent");
	if(checkCookie != "true"){
		setCookie("li_cookie_consent", "true", 31557600);
	}
	fadeOutWindow();
}
function fadeOutWindow(){
	var fadeTarget = document.getElementById("cookieBox");
	var fadeEffect = setInterval(function () {
			if (!fadeTarget.style.opacity) {
					fadeTarget.style.opacity = 1;
			}
			if (fadeTarget.style.opacity > 0) {
					fadeTarget.style.opacity -= 0.1;
			} else {
					clearInterval(fadeEffect);
					fadeTarget.style.display = 'none';
			}
	}, 25);
}
function setCookie(cookieName, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cookieName + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cookieName) {
  var name = cookieName + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var sortedCookies = decodedCookie.split(';');
  for(var i = 0; i < sortedCookies.length; i++) {
    var c = sortedCookies[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
manageCookie();
