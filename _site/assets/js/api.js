function submitToAPI(e) {
	e.preventDefault();
	var URL = "https://8e37f2cq45.execute-api.eu-central-1.amazonaws.com/prod";

	var name = document.getElementById("inputName").value;
	var email = document.getElementById("inputEmail").value;
	var desc = document.getElementById("inputText").value;

	var data = {
		name: name,
		email: email,
		desc: desc
	};
	var xhttp = new XMLHttpRequest();
	var jsonData = JSON.stringify(data);
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == XMLHttpRequest.DONE) { // XMLHttpRequest.DONE == 4
			if (xhttp.status == 200) {
				document.getElementById("contactForm").innerHTML = xhttp.responseText;
			} else if (xhttp.status == 400) {
				alert('There was an error 400');
			} else {
				alert('something else other than 200 was returned');
			}
		}
	};

	xhttp.open("POST", "https://8e37f2cq45.execute-api.eu-central-1.amazonaws.com/prod", true);
	xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
	console.log(jsonData);
	xhttp.send(jsonData);
	// or we can send this if we don't want to send json format
	// xhttp.send("?name=" + name + "?email=" + email + "?text=" + desc);
}
