---
layout: default
permalink: /about
title: About - Stack Center
heroIcon: /assets/img/icon4.svg
heroTitle: About us
heroSubtitle: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
teamTitle: Our Team
accessCta: Get early access
members:
  - name: Senka Ibrahimpasic
    role: Frontend Developer
    linkedin: https://www.linkedin.com/in/senka-ibrahimpašić-18a653141/
    img: /assets/img/user.svg
  - name: Isaac Nebot
    role: Consultant
    linkedin: https://www.linkedin.com/in/isaac-nebot-4b686958/
    img: /assets/img/user.svg
  - name: Istvan Szukacs
    role: Managing Director & Lead Consulant
    linkedin: https://www.linkedin.com/in/iszukacs/
    img: /assets/img/user.svg
  - name: Gabor Gergely
    role: Consultant
    linkedin: https://www.linkedin.com/in/ggergely/
    img: /assets/img/user.svg
  - name: You?
    role: Would you like to join us?
    linkedin: https://www.linkedin.com
    img: /assets/img/user.svg
contact:
  title: Contact Us
  placeholderName: Your Name
  placeholderMail: Your Email
  placeholderText: Your Message
accessBanner:
  title: Start transforming your software architecture now
---
{% include_relative _layouts/about.html content=page %}
