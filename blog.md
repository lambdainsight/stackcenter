---
layout: default
permalink: /blog
title: Blog - Stack Center
heroTitle: Welcome to our blog
heroSubtitle: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
postsTitle: Posts
accessCta: Get early access
posts:
  - date: Dec 12, 2019
    title: Getting our site up
    url: /blog/2019/12/12/welcome-to-lambdainsight.html
    text: Advising, for those who can source internally and need some direction, motivation, confirmation. Executing for those who struggle sourcing inhouse, because of resource constraints
  - date: Dec 12, 2019
    title: Getting our site up
    url: /blog/2019/12/12/welcome-to-lambdainsight.html
    text: Advising, for those who can source internally and need some direction, motivation, confirmation. Executing for those who struggle sourcing inhouse, because of resource constraints
  - date: Dec 12, 2019
    title: Getting our site up
    url: /blog/2019/12/12/welcome-to-lambdainsight.html
    text: Advising, for those who can source internally and need some direction, motivation, confirmation. Executing for those who struggle sourcing inhouse, because of resource constraints
  - date: Dec 12, 2019
    title: Getting our site up
    url: /blog/2019/12/12/welcome-to-lambdainsight.html
    text: Advising, for those who can source internally and need some direction, motivation, confirmation. Executing for those who struggle sourcing inhouse, because of resource constraints
accessBanner:
  title: Start transforming your software architecture now
---
{% include_relative _layouts/blog.html content=page %}
