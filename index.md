---
layout: default
title: Index - Stack Center
heroTitle: Software architecture as code
subtitle: StackCenter allows you to effortlessly design, document and review software architecture in a collaborative way
accessCta: Get early access
joinTeam: Join the team
architectureTitle: Software architecture done better
architectureSubtitle: StackCenter provides a simple, visual and collaborative way to create, share and document your software architecture, and manage it throughout its entire life-cycle, leading to a more aligned, more productive and better performing tech organization.
rowBlocks1:
  - title: Seamless workflow and collaboration
    text: Simplify your architecture workflow and have multiple team members collaborate on your architecture design and documentation in real-time. Use one simple purpose-built programming interface instead of a myriad of generic tools.
  - title: Managing multiple constraints with ease
    text: Make the best architecture decisions given your constraints by understanding security, performance and cost implications of different models on-the-go.
  - title: Version controlled, evolving designs in one repository
    text: Track changes within projects easily or follow the evolution of your entire stack over time. Ensure better knowledge transfer among engineering teams and accelerate the learning curve of new hires or junior team members.
rowBlocks2:
  - title: Internal and external community support
    text: Tap into the knowledge of your organization or the broader architect community. Search among already existing designs or ask for specific help on architecture or system-related questions.
  - title: Better review and stakeholder management
    text: Create better alignment between different stakeholders throughout the software design life-cycle and let your architecture pass through security and budget reviews easily.
  - title: Consistent architectural framework for your organization
    text: Implement and manage a consistent architectural framework for system and software design at any stage of your company. Improve operational excellence and achieve better security, reliability and performance across the organization.

accessBanner:
  title: Start transforming your software architecture now

featuresTitle: Features highlight
features:
  - icon: /assets/img/icons/icon1.svg
    text: Code-based interface
  - icon: /assets/img/icons/icon2.svg
    text: Built-in chart editor
  - icon: /assets/img/icons/icon3.svg
    text: Visual version control
  - icon: /assets/img/icons/icon4.svg
    text: Team and collaboration features
  - icon: /assets/img/icons/icon5.svg
    text: Pre-built document templates
  - icon: /assets/img/icons/icon6.svg
    text: Diagram library
  - icon: /assets/img/icons/icon7.svg
    text: External community support
  - icon: /assets/img/icons/icon8.svg
    text: Integration with engineering tools (GitHub, Slack etc.)
  - icon: /assets/img/icons/icon9.svg
    text: Output reports (architecture presentation, cost analysis, etc)
  - icon: /assets/img/icons/icon10.svg
    text: Automated deployment
  - icon: /assets/img/icons/icon11.svg
    text: Architecture catalogue

---
{% include_relative _layouts/index.html content=page %}
