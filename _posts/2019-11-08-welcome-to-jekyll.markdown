---
layout: default
postTitle:  "Welcome to Jekyll!"
postDate:   2019-12-12 12:12:12 +0100
categories: blog
postContent:
  - text: 'Using lorem ipsum to focus attention on graphic elements in a webpage design proposal
	In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used before final copy is available, but it may also be used to temporarily replace copy in a process called greeking, which allows designers to consider form without the meaning of the text influencing the design.'
  - img: '/assets/img/architecture.svg'
  - text: 'Using lorem ipsum to focus attention on graphic elements in a webpage design proposal
  In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used before final copy is available, but it may also be used to temporarily replace copy in a process called greeking, which allows designers to consider form without the meaning of the text influencing the design.'
  - img: '/assets/img/function.png'
  - text: 'Using lorem ipsum to focus attention on graphic elements in a webpage design proposal
	In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used before final copy is available, but it may also be used to temporarily replace copy in a process called greeking, which allows designers to consider form without the meaning of the text influencing the design.'

---

{% include_relative /post.html content=page %}
